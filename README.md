# Maven_to_Tomcat_Deployment

Setting up manager role user credentials for Tomcat
*****************************************************
File Location: /opt/tomcat/apache-tomcat-9.0.21/conf
File Name: tomcat-users.xml (Take a back up of existing file before editing)
Content Addition:-
<role rolename="manager-gui"/>
<role rolename="manager-script"/>
<user username="jenkinslocal" password="jenkinslocal" roles="manager-gui, manager-script"/>

Setting up same credentials in maven settings as defined above (in this case - jenkinslocal)
*********************************************************************************************
File Location: /usr/share/maven/conf
File Name: settings.xml (Take a back up of existing file before editing)
Content Addition:-
<servers>
    <server>
	<id>TomcatServer</id>
	<username>jenkinslocal</username>
	<password>jenkinslocal</password>
    </server>	
</servers>

Adding tomcat plugin in pom.xml file under build tag i.e. <build>
******************************************************************
File Location: /usr/share/maven/bin/tomcat-war-deployment
File Name: pom.xml
Content Addition:-
<build>
    <finalName>tomcat-war-deployment</finalName>
        <plugins>
            <plugin>
            <groupId>org.apache.tomcat.maven</groupId>
		    <artifactId>tomcat7-maven-plugin</artifactId>
	        <version>2.2</version>
                 <configuration>
        		<url>http://localhost:9090/manager/text</url>
        		<server>TomcatServer</server>
        		<path>/myapp</path>
    		    </configuration>
            </plugin>
        </plugins>
</build>
  
Please note: Port should be as per your m/c in which tomcat is running and you can see the webpage at /myapp after login to "Manager App" in top right side of Tomcat Welcome Page

Build Commands
***************
mvn tomcat7:deploy (on successful build message, you could see /myapp folder created under "Manager App"
mvn tomcat7:undeploy (on successful build message, /myapp folder would disappear)
mvn tomcat7:redeploy (on successful build message, /myapp folder would re-appear)

